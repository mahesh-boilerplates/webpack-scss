import 'angular';
import 'angular-mocks';
import "@uirouter/angularjs";
import "oclazyload";
import "angular-scroll";
import "angular-messages";
import "angular-credit-cards";
import "ngstorage";

/**
 * Requires: create file ending with .spec.js inside ./src/modules
 * Rest, will do by script automatically.
 */

import './src/index';

let context = require.context('./src/modules', true, /\.spec\.js/);
context.keys().forEach(context);