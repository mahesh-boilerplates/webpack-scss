const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common');
require('dotenv').config();

module.exports = merge(common, {
    devtool: 'eval',
    devServer: {
        contentBase: './dist',
        hot: true
    },
    module: {
        noParse: /node_modules|jquery|lodash/,
        rules: [{
            test: /\.less$/,
            exclude: /node_modules/,
            use: [
                'style-loader',
                'css-loader',
                'less-loader'
            ]
        }]
    },
    plugins: [
        new webpack.DefinePlugin({
            'process.env.API': JSON.stringify(process.env.API)
        }),
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin()
    ]
});