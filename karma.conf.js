const _webpack = require('./webpack.dev');

/**
 * Note: currently we are not using the main webpack config file into our test because there is commanChunkPlugin
 * Follow the following two lie of step if you want to add your main webpack config file.
 * let webpackConfig = require('./webpack.dev');
 * webpackConfig.plugins = []; // by overriding plugins, we remove CommonsChunkPlugin
 */
module.exports = function (config) {
    config.set({
        browsers: ['Chrome'],
        frameworks: ['jasmine'],
        reporters: ['spec'],
        logLevel: config.LOG_INFO,
        // autoWatch: true,
        singleRun: false,
        colors: true,
        port: 9876,
        basePath: '',
        files: ['./karma.context.js'],
        preprocessors: {'karma.context.js': ['webpack']},
        webpack: {
            module: _webpack.module
        },
        webpackMiddleware: {
            noInfo: true
        }
    });
};