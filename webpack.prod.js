const webpack = require('webpack');
const merge = require('webpack-merge');
const common = require('./webpack.common');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');

module.exports = merge(common, {
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.scss/,
                exclude: /node_modules/,
                use : ExtractTextPlugin.extract({
                    fallback : "style-loader",
                    use : [
                        {
                            loader : "css-loader"
                        },
                        {
                            loader : "sass-loader"
                        }
                    ]
                })
            },
        ]
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: false,
            mangle: {
                // You can specify all variables that should not be mangled.
                // For example if your vendor dependency doesn't use modules
                // and relies on global variables. Most of angular modules relies on
                // angular global variable, so we should keep it unchanged
                except: ['$super', '$', 'exports', 'require', 'angular']
            }
        }),
        new ExtractTextPlugin("styles.css"),
        new FaviconsWebpackPlugin('./src/img/favicon.png')
    ]
});
